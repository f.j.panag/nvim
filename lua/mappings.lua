local wk = require("which-key")

-- Neovim
wk.add({
    { "<leader>e", group = "Editor" },
    { "<leader>eh", "<cmd>IlluminateToggle<cr>", desc = "Toggle highlight" },
    { "<leader>ec", "<cmd>noh<cr>", desc = "Clear highlight" },
    { "<leader>el", function() vim.api.nvim_set_option_value("colorcolumn", vim.api.nvim_get_option_value("colorcolumn", {}) == "" and "120" or "", {}) end, desc = "Toggle colorcolumn" },
    { "<leader>ew", "<cmd>set wrap!<cr>", desc = "Toggle line wrap" },
    { "<leader>es", function() vim.opt.spell = not(vim.opt.spell:get()) end, desc = "Toggle spell check" },
    { "<leader>er", "<cmd>cd %:p:h<cr>", desc = "Set CWD" },
    { "<leader>eq", "<cmd>Bdelete<cr>", desc = "Close buffer" },
    { "<leader>]", "<cmd>bnext<cr>", desc = "Next buffer" },
    { "<leader>[", "<cmd>bprev<cr>", desc = "Previous buffer" },
    { "<leader>q", function(bufnum) require("bufdelete").bufdelete(bufnum, true) end, desc = "Close buffer" }
})

wk.add({
    { "<C-tab>", "<cmd>bnext<cr>", desc = "Next buffer" },
    { "<C-S-tab>", "<cmd>bprev<cr>", desc = "Previous buffer" }
})

-- Neotree
wk.add({
    { "\\", "<cmd>Neotree reveal left<cr>", desc = "Neo-tree" }
})

wk.add({
    { "<leader>E", "<cmd>Neotree toggle<cr>", desc = "Neo-tree" }
})


-- MiniMap
wk.add({
    { "<leader>em", function() MiniMap.toggle() end, desc = "Toggle minimap" }
})


-- Session Manager
wk.add({
    { "<leader>S", group = "Session" },
    { "<leader>Ss", "<cmd>SessionManager! save_current_session<cr>", desc = "Save session" },
    { "<leader>Sl", "<cmd>SessionManager! load_last_session<cr>", desc = "Last session" },
    { "<leader>Sf", "<cmd>SessionManager! load_session<cr>", desc = "Load session" },
    { "<leader>Sd", "<cmd>SessionManager! delete_session<cr>", desc = "Delete session" },
    { "<leader>S.", "<cmd>SessionManager! load_current_dir_session<cr>", desc = "Load current directory session" }
})


-- LSP
wk.add({
    { "<F1>", function() require("hover").hover() end, desc = "LSP hover" }
})

wk.add({
    { "<leader>l", group = "LSP" },
    { "<leader>ls", "<cmd>LspStart<cr>", desc = "Start LSP" },
    { "<leader>lS", "<cmd>LspStop<cr>", desc = "Stop Lsp" },
    { "<leader>lr", "cmd>LspRestart<cr>", desc = "Restart LSP" },
    { "<leader><tab>", "<cmd>ClangdSwitchSourceHeader<cr>", desc = "Toggle source - header" }
})


-- Telescope
wk.add({
    { "<leader>f", group = "Find" },
    { "<leader>ff", function() require("telescope.builtin").find_files() end, desc = "Find file" },
    { "<leader>fg", function() require("telescope.builtin").live_grep() end, desc = "Live grep" },
    { "<leader>fb", function() require("telescope.builtin").buffers() end, desc = "Find buffer" },
    { "<leader>fh", function() require("telescope.builtin").help_tags() end, desc = "Help tags" }
})


-- Git
local lazygit = require("toggleterm.terminal").Terminal:new({ cmd = "lazygit", hidden = true, direction = "float", start_in_insert = true, close_on_exit = true })
function _lazygit_toggle() lazygit:toggle() end

wk.add({
    { "[g", function() require("gitsigns").prev_hunk() end, desc = "Previous hunk" },
    { "]g", function() require("gitsigns").next_hunk() end, desc = "Next hunk" }
})

wk.add({
    { "<leader>g", group = "Git" },
    { "<leader>gg", "<cmd>lua _lazygit_toggle()<CR>", desc = "LazyGit" },
    { "<leader>gl", group = "GitSigns" },
    { "<leader>glc", "<cmd>Gitsigns toggle_signs<cr>", desc = "Toggle signs column" },
    { "<leader>gln", "<cmd>Gitsigns toggle_numhl<cr>", desc = "Toggle line number highlight" },
    { "<leader>gll", "<cmd>Gitsigns toggle_linehl<cr>", desc = "Toggle line highlight" },
    { "<leader>glw", "<cmd>Gitsigns toggle_word_diff<cr>", desc = "Toggle word diff" },
    { "<leader>gb", function() require("gitsigns").toggle_current_line_blame() end, desc = "Toggle line blame" },
    { "<leader>gs", function() require("gitsigns").stage_hunk() end, desc = "Stage hunk" },
    { "<leader>gS", function() require("gitsigns").stage_buffer() end, desc = "Stage buffer" },
    { "<leader>gu", function() require("gitsigns").undo_stage_hunk() end, desc = "Unstage hunk" },
    { "<leader>gr", function() require("gitsigns").reset_hunk() end, desc = "Reset hunk" },
    { "<leader>gR", function() require("gitsigns").reset_buffer() end, desc = "Reset buffer" },
    { "<leader>gp", function() require("gitsigns").preview_hunk() end, desc = "Preview hunk" },
    { "<leader>gd", function() require("gitsigns").toggle_deleted() end, desc = "Toggle deleted" },
    { "<leader>gD", function() require("gitsigns").diffthis() end, desc = "Diff" }
})

wk.add({
	{
		mode = { "v" },
		{ "<leader>g", group = "Git" },
		{ "<leader>gs", function() require("gitsigns").stage_hunk({vim.fn.line('.'), vim.fn.line('v')}) end, desc = "Stage hunk" },
		{ "<leader>gu", function() require("gitsigns").undo_stage_hunk({vim.fn.line('.'), vim.fn.line('v')}) end, desc = "Unstage hunk" },
		{ "<leader>gr", function() require("gitsigns").reset_hunk({vim.fn.line('.'), vim.fn.line('v')}) end, desc = "Reset hunk" }
	}
})

wk.add({
    { "ih", "<cmd><C-U>Gitsigns select_hunk<cr>", desc = "Select hunk", mode = { "o", "x" } }
})


-- DAP
wk.add({
	{
		{ "<leader>d", group = "Debug" },
		{ "<F5>", function() require("dap").step_into() end, desc = "Step into" },
		{ "<F6>", function() require("dap").step_over() end, desc = "Step over" },
		{ "<F7>", function() require("dap").step_out() end, desc = "Step out" },
		{ "<F8>", function() require("dap").continue() end, desc = "Continue" },
		{ "<leader>db", function() require("dap").toggle_breakpoint() end, desc = "Breakpoint" },
		{ "B", function() require("dap").toggle_breakpoint() end, desc = "Breakpoint" },
		{ "<leader>dg", function() require("dap").run_to_cursor() end, desc = "Run to cursor" },
		{ "<leader>dp", function() require("dap").pause() end, desc = "Pause" },
		{ "<leader>dl", function() require("dap").list_breakpoints() end, desc = "List breakpoints" },
		{ "<leader>dc", function() require("dap").clear_breakpoints() end, desc = "Clear breakpoints" },
		{ "<leader>dr", function() require("dap").restart() end, desc = "Restart session" },
		{ "<leader>dt", function() require("dap").terminate() end, desc = "Terminate session" },
		{ "<leader>d\\", function() require("dap").focus_frame() end, desc = "Focus frame" },
		{ "<leader>dU", function() require("dapui").toggle() end, desc = "Toggle DAP UI" }
	}
})


-- Terminal
local Terminal  = require("toggleterm.terminal").Terminal

local lazydocker = require("toggleterm.terminal").Terminal:new({ cmd = "lazydocker", hidden = true, direction = "float", start_in_insert = true, close_on_exit = true })
function _lazydocker_toggle() lazydocker:toggle() end

local bashtop = require("toggleterm.terminal").Terminal:new({ cmd = "bashtop", hidden = true, direction = "float", start_in_insert = true, close_on_exit = true })
function _bashtop_toggle() bashtop:toggle() end

vim.keymap.set('t', '<C-esc>', [[<C-\><C-n>]])
vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]])

wk.add({
    { "<F12>", "<cmd>ToggleTerm<CR>", desc = "Toggle terminal", mode = { "i", "n", "t", "v" } }
})

wk.add({
    { "<leader>t", group = "Terminal" },
    { "<leader>tt", "<cmd>terminal<cr>", desc = "Tab terminal" },
    { "<leader>tg", "<cmd>! gnome-terminal<cr>", desc = "Gnome Terminal" },
    { "<leader>tm", function() os.execute('gnome-terminal --maximize -- minicom --color=on -D ' .. vim.fn.input('Port: ', '/dev/ttyUSB0', 'file')) end, desc = "Minicom" },
    { "<leader>td", "<cmd>lua _lazydocker_toggle()<cr>", desc = "LazyDocker" },
    { "<leader>tD", "<cmd>! gnome-terminal --maximize -- lazydocker<cr>", desc = "LazyDocker" },
    { "<leader>tb", "<cmd>lua _bashtop_toggle()<cr>", desc = "Bashtop" },
    { "<leader>tB", "<cmd>! gnome-terminal --maximize -- Bashtop<cr>", desc = "Bashtop" }
})


-- Vim notify
wk.add({
    { "<leader>en", "<cmd>Telescope notify<cr>", desc = "Editor notifications" }
})


-- Make
local menuconfig = Terminal:new({ cmd = "make menuconfig", hidden = true, direction = "float", start_in_insert = true, close_on_exit = true })
function _menuconfig_toggle() menuconfig:toggle() end

wk.add({
    { "<leader>m", group = "Make" },
    { "<leader>ma", '<cmd>TermExec cmd="make all"<cr>', desc = "make all" },
    { "<leader>mc", '<cmd>TermExec cmd="make clean"<cr>', desc = "make clean" },
    { "<leader>md", '<cmd>TermExec cmd="make distclean"<cr>', desc = "make distclean" },
    { "<leader>mm", "<cmd>lua _menuconfig_toggle()<CR>", desc = "make menuconfig" }
})

