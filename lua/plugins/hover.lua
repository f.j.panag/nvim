return {
	{
		"lewis6991/hover.nvim",
		priority = 50,
		lazy = true,
		config = function()
			require("hover").setup({
				init = function()
					require("hover.providers.lsp")
					-- require('hover.providers.gh')
					-- require('hover.providers.gh_user')
					-- require('hover.providers.jira')
					-- require('hover.providers.man')
					-- require('hover.providers.dictionary')
				end,
				preview_opts = {
					border = nil
				},
				preview_window = false,
				title = false
			})
		end
	}
}

