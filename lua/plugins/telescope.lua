return {
	{
		"nvim-telescope/telescope.nvim",
		priority = 50,
		lazy = false,
		tag = "0.1.2",
		dependencies = {
			"nvim-lua/plenary.nvim",
			{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" }
		},
		config = function()
			require("telescope").setup({
				defaults = {
					mappings = {
						i = {
							["<C-Down>"] = require("telescope.actions").cycle_history_next,
							["<C-Up>"] = require("telescope.actions").cycle_history_prev,
						},
						n = { q = require("telescope.actions").close }
					}
				},
				extensions = {
					fzf = {
						fuzzy = false,
						case_mode = "smart_case"
					}
				}
			})

			require('telescope').load_extension('fzf')
		end
	}
}

