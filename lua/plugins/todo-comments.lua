return {
	{
		"folke/todo-comments.nvim",
		priority = 50,
		lazy = false,
		dependencies = {
			"nvim-lua/plenary.nvim"
		},
		config = function()
			require("todo-comments").setup({
				highlight = {
					pattern = [[.*<(KEYWORDS)\s*]],
				}
			})
		end
	}
}

