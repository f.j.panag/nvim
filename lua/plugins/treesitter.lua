return {
	{
		"nvim-treesitter/nvim-treesitter",
		priority = 50,
		lazy = false,
		build = function()
			require("nvim-treesitter.install").update({ with_sync = true })
		end,
		dependencies = {
			"RRethy/nvim-treesitter-endwise",
			"windwp/nvim-ts-autotag"
		},
		config = function()
			require("nvim-treesitter.configs").setup({
				ensure_installed = {
					"c", "cpp", "lua", "luadoc",
					"make", "kconfig", "dockerfile", "bash", "vim", "vimdoc",
					"html", "http", "xml", "json", "yaml", "csv", "comment",
					"markdown", "markdown_inline", "mermaid",
					"diff", "gitcommit", "git_rebase", "gitattributes", "gitignore"
				},
				sync_install = false,
				auto_install = true,
				highlight = {
					enable = true,
					auto_install = true,
					additional_vim_regex_highlighting = false
				},
				endwise = { enable = true },
				autotag = {
					enable = true,
					enable_close = true,
					enable_close_on_slash = false,
					filetypes = { "html" , "xml" }
				}
			})
		end
	}
}

