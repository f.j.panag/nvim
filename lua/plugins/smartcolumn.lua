return {
	{
		"m4xshen/smartcolumn.nvim",
		priority = 50,
		lazy = false,
		config = function()
			require("smartcolumn").setup({
				colorcolumn = "120",
				scope = "file",
				disabled_filetypes = { 
					"alpha",
					"neo-tree",
					"checkhealth"
				}
			})
		end
	}
}

