return {
	{
		"rcarriga/nvim-dap-ui",
		priority = 50,
		lazy = true,
		event = "VeryLazy",
		dependencies = {
			"mfussenegger/nvim-dap",
			"nvim-neotest/nvim-nio"
		},
		config = function()
			require("dapui").setup()
		end
	}
}

