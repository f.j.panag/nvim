return {
	{
		"windwp/nvim-autopairs",
		priority = 50,
		lazy = true,
		event = "InsertEnter",
		config = function()
			require("nvim-autopairs").setup({
				check_ts = true,
				enable_check_bracket_line = true
			})
		end
	}
}

