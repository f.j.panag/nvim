return {
	{
		"numToStr/Comment.nvim",
		priority = 50,
		lazy = false,
		config = function()
			require('Comment').setup({
				sticky = true,
				toggler = {
					line = "<leader>/"
				},
				opleader = {
					line = "<leader>/"
				}
			})
		end
	}
}

