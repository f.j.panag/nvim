return {
	{
		"lewis6991/gitsigns.nvim",
		priority = 50,
		lazy = false,
		config = function()
			require("gitsigns").setup({
				current_line_blame_opts = { delay = 500 }
			})
		end
	}
}

