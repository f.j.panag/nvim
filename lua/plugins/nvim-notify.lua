return {
	{
		"rcarriga/nvim-notify",
		priority = 50,
		lazy = false,
		config = function()
			local notify = require("notify")
			notify.setup({
				render = "compact",
				stages = "fade_in_slide_out",
				top_down = false,
				timeout = 3000
			})

			vim.notify = notify
		end
	}
}

