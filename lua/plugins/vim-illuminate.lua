return {
	{
		"RRethy/vim-illuminate",
		priority = 50,
		lazy = false,
		config = function()
			require("illuminate").configure({
				delay = 100,
				filetypes_denylist = {
					"neo-tree",
					"alpha"
				}
			})
		end
	}
}

