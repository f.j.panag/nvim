return {
	{
		"echasnovski/mini.map",
		version = false,
		priority = 50,
		lazy = true,
		event = "BufNew",
		config = function()
			mini_map = require("mini.map")
			mini_map.setup({
				integrations = {
					mini_map.gen_integration.builtin_search(),
					mini_map.gen_integration.gitsigns()
				},
				symbols = {
					encode = mini_map.gen_encode_symbols.dot('4x2')
				},
				window = {
					width = 25,
					focusable = true,
					winblend = 70
				}
			})

			vim.cmd([[
				augroup MyMiniMapColors
					au!
					au ColorScheme * hi link MiniMapSymbolCount Special
					au ColorScheme * hi link MiniMapSymbolLine  SpecialKey
					au ColorScheme * hi link MiniMapSymbolView  Delimiter
				augroup END
			]])
		end
	}
}

