return {
	{
		"akinsho/toggleterm.nvim",
		priority = 50,
		lazy = false,
		config = function()
			require("toggleterm").setup({
				auto_scroll = true,
				autochdir = true,
				start_in_insert = true,
				insert_mappings = true,
				terminal_mappings = true,
				persist_mode = false,
				persist_size = false
			})
		end
	}
}

