return {
	{
		"Shatur/neovim-session-manager",
		priority = 50,
		lazy = false,
		dependencies = {
			"nvim-lua/plenary.nvim"
		},
		config = function()
			require("session_manager").setup({
				autoload_mode = require("session_manager.config").AutoloadMode.Disabled,
				autosave_last_session = true, 
				autosave_only_in_session = true,
				autosave_ignore_filetypes = { "gitcommit", "gitrebase", "toggleterm" },
				autosave_ignore_buftypes = { "terminal" }
			})
		end
	}
}

