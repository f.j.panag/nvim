return {
	{
		"akinsho/bufferline.nvim",
		priority = 50,
		lazy = true,
		dependencies = {
			"nvim-tree/nvim-web-devicons",
			"famiu/bufdelete.nvim"
		},
		config = function()
			require("bufferline").setup({
				options = {
					mode = "buffers",
					numbers = "none",
					close_command = function(bufnum) require("bufdelete").bufdelete(bufnum, true) end,
					right_mouse_command = "",
					left_mouse_command = "buffer %d",
					middle_mouse_command = function(bufnum) require("bufdelete").bufdelete(bufnum, true) end,
					indicator = {
						style = "underline"
					},
					buffer_close_icon = "",
					modified_icon = "●",
					close_icon = "",
					left_trunc_marker = "",
					right_trunc_marker = "",
					max_name_length = 18,
					max_prefix_length = 15,
					truncate_names = true,
					tab_size = 25,
					diagnostics = false,
					offsets = {
						{
							filetype = "neo-tree",
							text = "Neovim v" .. vim.fn.matchstr(vim.fn.execute "version", "NVIM v\\zs[^\n]*"),
							text_align = "left",
							separator = false
						}
					},
					color_icons = false,
					show_buffer_icons = true,
					show_buffer_close_icons = true,
					show_close_icon = true,
					show_tab_indicators = true,
					show_duplicate_prefix = true,
					persist_buffer_sort = true,
					separator_style = "thin",
					enforce_regular_tabs = false,
					always_show_bufferline = true,
					hover = {
						enabled = true,
						delay = 50,
						reveal = { "close" }
					},
					sort_by = "insert_at_end"
				},
				-- highlights = require("nord").bufferline.highlights({ italic = true, bold = true })
			})
		end
	}
}

