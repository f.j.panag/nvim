return {
	{
		"williamboman/mason.nvim",
		priority = 60,
		lazy = false,
		config = function()
			require("mason").setup({
				ui = {
					icons = {
						package_installed = "✓",
						package_uninstalled = "✗",
						package_pending = "⟳",
					}
				}
			})
		end
	}
}

