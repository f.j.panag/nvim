return {
	{
		"j-hui/fidget.nvim",
		priority = 50,
		tag = "legacy",
		lazy = true,
		event = "LspAttach",
		config = function()
			require("fidget").setup()
		end
	}
}

