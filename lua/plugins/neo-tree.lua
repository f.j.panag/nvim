return {
	{
		"nvim-neo-tree/neo-tree.nvim",
		priority = 50,
		enabled = not vim.g.started_by_firenvim,
		lazy = false,
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim"
		},
		config = function()
			vim.fn.sign_define("DiagnosticSignError", { text = " ", texthl = "DiagnosticSignError" })
			vim.fn.sign_define("DiagnosticSignWarn",  { text = " ", texthl = "DiagnosticSignWarn"  })
			vim.fn.sign_define("DiagnosticSignInfo",  { text = " ", texthl = "DiagnosticSignInfo"  })
			vim.fn.sign_define("DiagnosticSignHint",  { text = "",  texthl = "DiagnosticSignHint"  })

			require("neo-tree").setup({
				close_if_last_window = false,
				enable_git_status = true,
				enable_diagnostics = false,
				sort_case_insensitive = false,

				default_component_configs = {
					name = {
						trailing_slash = false,
						use_git_status_colors = true
					},
					indent = {
						padding = 0
					},
					icon = {
						folder_closed = "",
						folder_open = "",
						folder_empty = "",
						folder_empty_open = "",
						default = "󰈙",
					},
					modified = {
						symbol = "",
						-- symbol = "●",
					},
					git_status = {
						symbols = {
							added = "",
							deleted = "",
							modified = "",
							renamed = "➜",
							untracked = "★",
							ignored = "◌",
							-- unstaged = "✗",
							unstaged = "",
							staged = "✓",
							conflict = "",
						}
					}
				},

				window = {
					position = "left",
					width = 40,
					mapping_options = {
						noremap = true,
						nowait = true,
					},
					mappings = {
						["<space>"] = false
					}
				},

				filesystem = {
					follow_current_file = {
						enabled = false
					},
					group_empty_dirs = false,
					use_libuv_file_watcher = true,
					filtered_items = {
						visible = false,
						hide_dotfiles = true,
						hide_gitignored = false,
						hide_hidden = true,
						hide_by_name = { },
						hide_by_pattern = { },
						always_show = {
							".gitlab-ci.yml",
							".gitignore",
						},
						never_show = {
							".git"
						},
						never_show_by_pattern = {
							"*.o",
							"*.a",
							"*.d",
							"*.su",
							"*.gcno",
							"*.gcda"
						}
					}
				},

				event_handlers = {
					{
						event = "neo_tree_buffer_enter",
						handler = function(_) vim.opt_local.signcolumn = "auto" end
					},
					{
						event ="neo_tree_window_after_open",
						once = true,
						handler = function(_)
							vim.opt_local.number = false
							require("bufferline")
						end
					}
				}
			})
		end
	}
}

