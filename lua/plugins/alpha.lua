return {
	{
		"goolord/alpha-nvim",
		priority = 50,
		lazy = false,
		dependencies = {
			"nvim-tree/nvim-web-devicons"
		},
		config = function()
			local alpha = require("alpha")
			local dashboard = require("alpha.themes.dashboard")

			-- Set header
			dashboard.section.header.val = {
				[[      .-.             -.]],
				[[    .-+++-            =+-.]],
				[[  .-++++++=           =+++-.]],
				[[ ..=+++++++=.         =+++++-.]],
				[[--- -++++++++:        =++++++=]],
				[[----.:++++++++-       =++++++=]],
				[[-----:.=+++++++=.     =++++++=]],
				[[-------.-++++++++:    =+++++++]],
				[[-------- :++++++++-   =+++++++]],
				[[--------  .=+++++++=. =+++++++]],
				[[--------    =++++++++::+++++++]],
				[[--------     -++++++++-.=+++++]],
				[[--------      .=+++++++=.-++++]],
				[[--------        =++++++++::+++]],
				[[:-------         -++++++++-.=-]],
				[[  :-----          .=+++++++:]],
				[[    :---            =++++-]],
				[[      :-             -+-]]
			}

			-- Set menu
			dashboard.section.buttons.val = {
				dashboard.button( "f", "  > New file" ,	":ene<CR>"												),
				dashboard.button( "s", "  > Sessions" ,	":SessionManager! load_session<CR>"						),
				dashboard.button( "l", "  > Lazy.nvim",	":Lazy<CR>"												),
				dashboard.button( "m", "  > Mason",		":Mason<CR>"											),
				dashboard.button( "c", "  > Config",		":e $MYVIMRC | :cd %:p:h | :pwd | :Neotree show<CR>"	),
				dashboard.button( "t", "  > Terminal",		":! gnome-terminal<CR>"									),
				dashboard.button( "q", "  > Quit",			":qa<CR>"												),
			}

			-- Footer
			local function footer()
				local version = vim.version()
				local nvim_version_info = "Neovim v" .. version.major .. "." .. version.minor .. "." .. version.patch

				local stats = require("lazy").stats()
				local load_time = math.floor(stats.startuptime * 100 + 0.5) / 100

				return nvim_version_info .. " 💀 " .. stats.count .. " plugins - " .. load_time .. "ms" 
			end

			dashboard.section.footer.val = footer()
			dashboard.section.footer.opts.hl = "Comment"

			-- Layout
			dashboard.config.layout = {
				{ type = "padding", val = vim.fn.max({ 2, vim.fn.floor(vim.fn.winheight(0) * 0.2) }) },
				dashboard.section.header,
				{ type = "padding", val = 5 },
				dashboard.section.buttons,
				{ type = "padding", val = 1 },
				dashboard.section.footer,
			}

			-- Disable auto commands.
    		dashboard.config.opts.noautocmd = true
			
			-- Send config to alpha
			alpha.setup(dashboard.opts)

			-- Disable folding on alpha buffer
			vim.cmd([[autocmd FileType alpha setlocal nofoldenable]])

			-- Refresh start-up time after loading.
			vim.api.nvim_create_autocmd("User", {
				pattern = "LazyVimStarted",
				desc = "Alpha dashboard footer",
				once = true,
				callback = function()
						dashboard.section.footer.val = footer()
						pcall(vim.cmd.AlphaRedraw)
					end
				})

		end
	}
}

