return {
	{
		"utilyre/barbecue.nvim",
		priority = 50,
		lazy = false,
		dependencies = {
			"SmiteshP/nvim-navic",
			"nvim-tree/nvim-web-devicons"
		},
		config = function()
			require("barbecue").setup({
				exclude_filetypes = { "alpha", "neo-tree", "netrw", "toggleterm" }
			})
		end
	}
}

