return {
	{
		"folke/which-key.nvim",
		priority = 50,
		lazy = false,
		dependencies = {
			"echasnovski/mini.icons"
		},
		config = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300

			require("which-key").setup({
				preset = "clasic",
				win = {
					no_overlap = true,
					title = true,
					title_pos = "center",
					border = "none",
					zindex = 1000,
					padding = { 1, 2 },
					col = 0.5,
					bo = {},
					wo = {
						winblend = 10,
					},
				},
				layout = {
					align = "center"
				},
				disable = {
					buftypes = {},
					filetypes = { "alpha" }
				},
				plugins = {
					marks = true,
					registers = true,
					spelling = { enabled = true }
				}
			})
		end
	}
}

