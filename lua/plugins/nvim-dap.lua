return {
	{
		"mfussenegger/nvim-dap",
		priority = 50,
		lazy = true,
		event = "VeryLazy",
		config = function()
			local dap = require("dap")

			dap.adapters.gdb =
			{
				type = "executable",
				command = "gdb",
				args = { "--interpreter=dap", "--eval-command", "set print pretty on" }
			}

			dap.configurations.c =
			{
				{
					name = "Segger JLink Debugger",
					type = "gdb",
					request = "attach",
					target = "localhost:2331",
					program = function()
						return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/bin/", "file")
					end,
					cwd = "${workspaceFolder}",
					on_config = function(config)
						vim.fn.jobstart("/opt/SEGGER/JLink/JLinkGDBServer -device " .. vim.fn.input("MCU: ", "STM32F427VI"))
						return config
					end
				}
			}
		end
	}
}

