return {
	{
		"nvim-lualine/lualine.nvim",
		priority = 50,
		enabled = not vim.g.started_by_firenvim,
		lazy = false,
		dependencies = {
			"nvim-tree/nvim-web-devicons"
		},
		config = function()
			require("lualine").setup({
				options = {
					theme = "nord",
					globalstatus = true,
					icons_enabled = true,
					section_separators = { left = "", right = "" },
					component_separators = { left = "", right = "" },
					disabled_filetypes = {
						statusline = { "alpha" }
					}
				},
				sections = {
					lualine_a = { "mode" },
					lualine_b = { "branch", { "diff", symbols = { added = " ", modified = " ", removed = " " } } },
					lualine_c = { { "filename", symbols = { modified = "", readonly = "◌" }, separator = "" }, "diagnostics", require("dap").status() },
					lualine_x = { "encoding", "filetype" },
					lualine_y = { --[[ { "searchcount", separator = "" }, --]] "progress" },
					lualine_z = { "location" }
				},
				extensions = { "neo-tree", "lazy", "nvim-dap-ui", "toggleterm" }
			})
		end
	}
}

