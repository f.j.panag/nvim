return {
	{
		"stevearc/dressing.nvim",
		priority = 50,
		lazy = false,
		config = function()
			require("dressing").setup({
				input = {
					default_prompt = "➤ ",
					border = "single",
					win_options = { winhighlight = "Normal:Normal,NormalNC:Normal" },
				},
				select = {
					backend = { "telescope", "builtin" },
					border = "single",
					builtin = { win_options = { winhighlight = "Normal:Normal,NormalNC:Normal" } },
				}
			})
		end
	}
}

