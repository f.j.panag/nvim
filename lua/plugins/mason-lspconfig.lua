return {
	{
		"williamboman/mason-lspconfig.nvim",
		priority = 60,
		lazy = false,
		dependencies = {
			"williamboman/mason.nvim",
			"williamboman/mason-lspconfig.nvim",
			"hrsh7th/cmp-nvim-lsp"
		},
		config = function()
			require("mason-lspconfig").setup({
				automatic_installation = true,
				ensure_installed = { "clangd", "lua_ls", "html", "lemminx", "dockerls", "bashls", "gitlab_ci_ls" }
			})

			require("mason-lspconfig").setup_handlers({

				-- Default handler.
				function (server_name)
					require("lspconfig")[server_name].setup({
						capabilities = require("cmp_nvim_lsp").default_capabilities()
					})
				end
			})
		end
	}
}

