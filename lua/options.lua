-- UI options.
vim.opt.termguicolors = true
vim.opt.mouse = "a"
vim.opt.mousemoveevent = true


-- Editor options.
vim.opt.fileencoding = "utf-8"

vim.opt.number = true
vim.opt.relativenumber = false
vim.opt.cursorline = true
vim.opt.wrap = false

vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = false

vim.opt.showtabline = 0
vim.opt.cmdheight = 1
vim.opt.showmode = false
vim.opt.fillchars = { eob = " " }

vim.opt.clipboard = "unnamedplus"
vim.opt.undofile = true


-- Search & replace options.
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.inccommand = "nosplit"


-- Code folding.
vim.opt.foldcolumn = "1"
vim.opt.foldlevel = 99
vim.opt.foldlevelstart = 99
vim.opt.fillchars = [[eob: ,fold: ,foldopen:,foldsep:|,foldclose:]]
vim.opt.foldenable = true
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"


-- Spell checking.
vim.opt.spelllang = "en_us"
vim.opt.spell = false


-- Misc options.
vim.opt.signcolumn = "yes"
vim.opt.splitright = true

