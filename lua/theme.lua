pcall(function()
	vim.cmd.colorscheme("nord")
	vim.opt.termguicolors = true
	vim.opt.guifont = "RobotoMono_Nerd_Font:h10"
	vim.g.icons_enabled = true
end)

