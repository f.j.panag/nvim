local user_group = vim.api.nvim_create_augroup("user_autocmds", { clear = true })

-- Session Manager
vim.api.nvim_create_autocmd({ "User" }, {
	pattern = "SessionSavePost",
	desc = "Re-open Neo-Tree after session is saved.",
	group = user_group,
	callback = function()
		require("neo-tree.command").execute({ action = "show" })
	end
})

vim.api.nvim_create_autocmd({ "User" }, {
	pattern = "SessionLoadPost",
	desc = "Re-open Neo-Tree after session is loaded.",
	group = user_group,
	callback = function()
		require("neo-tree.command").execute({ action = "show" })
	end
})


-- MiniMap
vim.api.nvim_create_autocmd({ "BufNew" }, {
	pattern = "*.c, *.h, *.md, Makefile, Kconfig, *.lua",
	desc = "Open minimap.",
	group = user_group,
	callback = function()
		MiniMap.open()
	end
})


-- Git
vim.api.nvim_create_autocmd("TermClose", {
	pattern = "*lazygit*",
	desc = "Refresh Neo-Tree when closing lazygit.",
	group = user_group,
	callback = function()
		local manager_avail, manager = pcall(require, "neo-tree.sources.manager")
		if manager_avail then
			for _, source in ipairs { "filesystem", "git_status", "document_symbols" } do
				local module = "neo-tree.sources." .. source
				if package.loaded[module] then manager.refresh(require(module).name) end
			end
		end
	end
})

vim.api.nvim_create_autocmd("User", {
	pattern = "GitSignsUpdate",
	desc = "Refresh Neo-Tree on GitSigns operations.",
	group = user_group,
	callback = function()
		local manager_avail, manager = pcall(require, "neo-tree.sources.manager")
		if manager_avail then
			for _, source in ipairs { "filesystem", "git_status", "document_symbols" } do
				local module = "neo-tree.sources." .. source
				if package.loaded[module] then manager.refresh(require(module).name) end
			end
		end
	end
})


-- Terminal
vim.api.nvim_create_autocmd({ "TermOpen" }, {
	pattern = "*",
	desc = "Set local options in terminal buffers.",
	group = user_group,
	callback = function()
		require("neo-tree.command").execute({ action = "show" })
		vim.cmd("setlocal nonumber")
		vim.cmd("setlocal signcolumn=no")
		vim.cmd("startinsert")
	end
})

