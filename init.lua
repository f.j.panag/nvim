--------------------------------------------------------------------------------
-- Neovim Configuration
--------------------------------------------------------------------------------

-- Set the leader key.
vim.g.mapleader = " "

-- Enable the Lua loader.
if vim.loader and vim.fn.has "nvim-0.9.1" == 1 then vim.loader.enable() end


-- Load user options.
require("options")


-- Start lazy.nvim.
require("lazy_nvim")


-- Load the theme.
require("theme")

-- Load any key mappings.
require("mappings")

-- Load any auto commands.
require("autocmds")

